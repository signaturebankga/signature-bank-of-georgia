Established in 2005, Signature Bank of Georgia is the only locally owned and operated community bank headquartered in Sandy Springs, Georgia, one of the most affluent communities in the country. The bank offers a full range of business and consumer deposit products and loans.

Address: 6065 Roswell Rd, Suite 110, Sandy Springs, GA 30328, USA

Phone: 404-620-5579
